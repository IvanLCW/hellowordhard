//
//  HelloViewController.swift
//  HelloWorld
//
//  Created by Macintosh on 28/06/2016.
//  Copyright © 2016 Apptivity Lab. All rights reserved.
//

import UIKit

class HelloViewController: UIViewController {
    
    var hellolabel: UILabel!
    var hiButton: UIButton!
    var byeButton: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.whiteColor()
        
        hellolabel = UILabel(frame: CGRect(x: 20, y: 20, width: 330, height: 80))
        hellolabel.text = "       Hello World😱"
        hellolabel.backgroundColor = UIColor.clearColor()
        hellolabel.textColor = UIColor.blackColor()
        self.view.addSubview(hellolabel)
        
        
        hiButton = UIButton(frame: CGRect(x: 25, y: 150, width: 150, height: 50))
        hiButton.setTitle("Say Hi", forState: UIControlState.Normal)
        hiButton.layer.cornerRadius = 5
        hiButton.layer.borderWidth = 1

        hiButton.backgroundColor = UIColor.greenColor()
        hiButton.titleLabel?.textColor = UIColor.blackColor()
        hiButton.setTitleColor(UIColor.init(colorLiteralRed: 196, green: 0.3098, blue: 0.52, alpha: 1.0), forState: UIControlState.Normal)

        hiButton.addTarget(self, action: #selector(HelloViewController.sayHi), forControlEvents: UIControlEvents.TouchUpInside)
        
        
        self.view.addSubview(hiButton)
        
        
        byeButton = UIButton(frame: CGRect(x: 185, y: 150, width: 150, height: 50))
        byeButton.setTitle("Say Bye", forState: UIControlState.Normal)
        byeButton.layer.cornerRadius = 5
        byeButton.layer.borderWidth = 1

        byeButton.backgroundColor = UIColor.redColor()
        byeButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        byeButton.addTarget(self, action: #selector(HelloViewController.sayBye), forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(byeButton)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sayHi(){
        print("Say Hi to PC")
        let 笑脸: String = "ZOMG"
        hellolabel.text = "       Hi Now \(笑脸)"}
    
    func sayBye(){
        print("Say Bye to PC")
        hellolabel.text = "       Bye Bye Now \u{1F625}"}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
